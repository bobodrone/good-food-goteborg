# Good Food in Göteborg

## Meet:  

* Blackstone Linné  
https://www.google.com/maps/place/Blackstone+Steakhouse+Linn%C3%A9gatan/@57.6970433,11.9502669,16.37z/data=!4m5!3m4!1s0x464ff3388347a2b9:0x314e78e4957a680a!8m2!3d57.6958129!4d11.9512568

* Buenos Aires  
https://www.google.com/maps/place/Buenos+Aires/@57.6989836,11.9818528,17z/data=!4m12!1m6!3m5!1s0x464ff3701dafd1a3:0xcf9f1db58b1cf624!2sBuenos+Aires!8m2!3d57.6990223!4d11.9839799!3m4!1s0x464ff3701dafd1a3:0xcf9f1db58b1cf624!8m2!3d57.6990223!4d11.9839799

* Texas longhorn Linné  
https://www.google.com/maps/place/Texas+Longhorn/@57.6912719,11.9519688,18.75z/data=!4m12!1m6!3m5!1s0x464ff3701dafd1a3:0xcf9f1db58b1cf624!2sBuenos+Aires!8m2!3d57.6990223!4d11.9839799!3m4!1s0x465f9de0e888fd11:0x55d1ac41ae33fcb0!8m2!3d57.6910029!4d11.9524328


## Burgers

* Dine Burgers  
https://www.google.com/maps/place/DIN%C3%89+Burgers/@57.7045902,11.9596119,17z/data=!4m9!1m2!2m1!1sburgers!3m5!1s0x464ff32804da62ab:0x71330f0b0530ef8b!8m2!3d57.7055437!4d11.9668138!15sCgdidXJnZXJzWgkiB2J1cmdlcnOSARRoYW1idXJnZXJfcmVzdGF1cmFudJoBI0NoWkRTVWhOTUc5blMwVkpRMEZuU1VSSk5qY3pZVUozRUFF

* The Barn  
https://www.google.com/maps/place/The+Barn/@57.7045902,11.9596119,17z/data=!4m9!1m2!2m1!1sburgers!3m5!1s0x43e9b02a330e522b:0xdb5522d8309aacce!8m2!3d57.7044093!4d11.962751!15sCgdidXJnZXJzWgkiB2J1cmdlcnOSARRoYW1idXJnZXJfcmVzdGF1cmFudJoBJENoZERTVWhOTUc5blMwVkpRMEZuU1VNd2FqbFBVemxCUlJBQg

* 2112  
https://www.google.com/maps/place/Restaurant+2112/@57.704413,11.9597143,17z/data=!4m9!1m2!2m1!1sburgers!3m5!1s0x464ff36624ad2c3d:0xe369adfa7fa94329!8m2!3d57.7044129!4d11.9619024!15sCgdidXJnZXJzWgkiB2J1cmdlcnOSARRoYW1idXJnZXJfcmVzdGF1cmFudJoBI0NoWkRTVWhOTUc5blMwVkpRMEZuU1VSdmMwMUlOa2gzRUFF

* Bastard Burgers  
https://www.google.com/maps/place/Bastard+Burgers+G%C3%B6teborg+S%C3%B6dra+Larmgatan/@57.7032114,11.9636484,17z/data=!4m9!1m2!2m1!1sburgers!3m5!1s0x464ff3af9ba865db:0xf744ef89468384ae!8m2!3d57.7032114!4d11.9658371!15sCgdidXJnZXJzWgkiB2J1cmdlcnOSARRoYW1idXJnZXJfcmVzdGF1cmFudJoBI0NoWkRTVWhOTUc5blMwVkpRMEZuU1VONWNYWjFXVkpSRUFF

* Tugg  
https://www.google.com/maps/place/Tugg+Burgers/@57.7032114,11.9636484,17z/data=!4m9!1m2!2m1!1sburgers!3m5!1s0x464ff365d9b567b7:0xb7e964a1a9d06cd6!8m2!3d57.7053736!4d11.9656323!15sCgdidXJnZXJzWgkiB2J1cmdlcnOSARRoYW1idXJnZXJfcmVzdGF1cmFudJoBJENoZERTVWhOTUc5blMwVkpRMEZuU1VNd2NtRXpVbDlSUlJBQg


## Vietnamese

* Lilla Hanoi  
https://www.google.com/maps/place/Lilla+Hanoi/@57.7076449,11.9647094,17z/data=!4m9!1m2!2m1!1svietnamese+restaurant!3m5!1s0x464ff366877629eb:0xa33d6d030c709394!8m2!3d57.7092306!4d11.9649845!15sChV2aWV0bmFtZXNlIHJlc3RhdXJhbnRaFyIVdmlldG5hbWVzZSByZXN0YXVyYW50kgEVdmlldG5hbWVzZV9yZXN0YXVyYW50mgEjQ2haRFNVaE5NRzluUzBWSlEwRm5TVU0yYjB0bVdFTm5FQUU

* Vietnamhaket  
https://www.google.com/maps/place/Vietnamhaket/@57.6991227,11.9469317,17.58z/data=!4m12!1m6!3m5!1s0x464ff36a6ef50247:0x6c24e52f08da09d8!2sHermanos+TacoKiosk!8m2!3d57.6840568!4d11.915846!3m4!1s0x464ff3419016cdcb:0xfab3c625ac339b4a!8m2!3d57.6981378!4d11.9499501

* Xin Chao  
https://www.google.com/maps/place/Xin+Ch%C3%A0o+Restaurang/@57.7026579,11.9572528,17z/data=!4m9!1m2!2m1!1svietnamese+restaurant!3m5!1s0x464ff3c77372310d:0xdeb074a5aba6d981!8m2!3d57.702853!4d11.9618564!15sChV2aWV0bmFtZXNlIHJlc3RhdXJhbnRaFyIVdmlldG5hbWVzZSByZXN0YXVyYW50kgEVdmlldG5hbWVzZV9yZXN0YXVyYW50mgEkQ2hkRFNVaE5NRzluUzBWSlEwRm5TVVJoYm5CUGNIbFJSUkFC

## Mexican

* Tomtoms  
https://www.google.com/maps/place/TomToms+Burritos/@57.6965447,11.9498454,17.58z/data=!4m12!1m6!3m5!1s0x464ff3701dafd1a3:0xcf9f1db58b1cf624!2sBuenos+Aires!8m2!3d57.6990223!4d11.9839799!3m4!1s0x464ff341b2de7271:0xa2261bc9fdc2b56c!8m2!3d57.6962702!4d11.949209

* Hermanos  
https://www.google.com/maps/place/Hermanos+TacoKiosk/@57.684088,11.913602,17z/data=!3m1!4b1!4m5!3m4!1s0x464ff36a6ef50247:0x6c24e52f08da09d8!8m2!3d57.6840568!4d11.915846

* Puta madre  
https://www.google.com/maps/place/Puta+Madre/@57.7045845,11.9596119,17z/data=!4m9!1m2!2m1!1smexican+restaurant!3m5!1s0x464ff3663a444abb:0xbc92daa07e3c8d7f!8m2!3d57.7045838!4d11.961797!15sChJtZXhpY2FuIHJlc3RhdXJhbnRaFCISbWV4aWNhbiByZXN0YXVyYW50kgESbWV4aWNhbl9yZXN0YXVyYW50mgEjQ2haRFNVaE5NRzluUzBWSlEwRm5TVU52YTNGMWQweG5FQUU

## Dim Sum

* Dubbel Dubbel (Kastellgatan)  
https://www.google.com/maps/place/Dubbel+Dubbel/@57.6940979,11.9565357,17.67z/data=!4m8!1m2!2m1!1sburgers!3m4!1s0x464ff36b1e6c7a69:0x5f51e42a958f832f!8m2!3d57.6943463!4d11.9556587

* Dubbel Dubbel (Surbrunnsgatan)
https://www.google.com/maps/place/Dubbel+Dubbel/@57.7022852,11.9557331,17.38z/data=!4m8!1m2!2m1!1sburgers!3m4!1s0x464ff36b1e6c7a69:0x5610f6ac35f236ce!8m2!3d57.7030126!4d11.9560804

## Czech

* Gyllene Prag  
https://www.google.com/maps/place/Gyllene+Prag/@57.6924195,11.9523168,17z/data=!3m1!4b1!4m5!3m4!1s0x464ff315291ea38b:0xd780cfb5dbedb742!8m2!3d57.6924167!4d11.9545055  

* Schnitzelplatz  
https://www.google.com/maps/place/Schnitzelplatz+Lagerhuset/@57.7021409,11.9557008,17.08z/data=!4m8!1m2!2m1!1sburgers!3m4!1s0x464ff30ec7dffbc5:0xea9cdf3fecc81ac5!8m2!3d57.701924!4d11.9540074


## Pizza & Beer
* Brewers Bar  
https://www.google.com/maps/place/Brewers+Beer+Bar+Tredje+L%C3%A5nggatan/@57.6991227,11.9469317,17.58z/data=!4m12!1m6!3m5!1s0x464ff36a6ef50247:0x6c24e52f08da09d8!2sHermanos+TacoKiosk!8m2!3d57.6840568!4d11.915846!3m4!1s0x464ff3418b6a5a15:0xa9bf77c8dfbbf99c!8m2!3d57.6986766!4d11.9509084
 
## Thai

## Indian

## Greek

## Italian

## Swedish

## Fish
